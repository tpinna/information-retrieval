# Information retrieval project

The project is an information retrieval approach to automatically retrieving a relevant XKCD or imggur.

# Installation
```sh
$ pip3 install [distribution.tgz]
```

### Usage
```sh
$ xkcd -h
```

### Development
This section contains info for developers. When you installed using pip you can ignore this.
#### Directory structure
```
relevantXKCD
 | - xkcd                   raw xkcd dataset
 | - index                  generated index (formerly 'testoutput')
 | - relevantXKCD.py        main() for xkcd command line tool (packaged) 
 | - relevantXKCD_dev.py    main() for developer command line tool (not packaged)
 | ...
xkcd_dev.py                 convenience wrapper for running developer cli
xkcd.py                     convenience wrapper for running xkcd cli
setup.py                    python setup script (see doc of python setuptools for more info)
...
```

#### Creating a distributable package
```sh
$ ./setup.py sdist
```




