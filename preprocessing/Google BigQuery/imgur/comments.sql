-- Comments:
--     1) parent of a Reaction

SELECT * FROM
(
SELECT
	*
FROM
  [fh-bigquery:reddit_comments.2007],
  [fh-bigquery:reddit_comments.2008],
  [fh-bigquery:reddit_comments.2009],
  [fh-bigquery:reddit_comments.2010],
  [fh-bigquery:reddit_comments.2011],
  [fh-bigquery:reddit_comments.2012],
  [fh-bigquery:reddit_comments.2013],
  [fh-bigquery:reddit_comments.2014],
  [fh-bigquery:reddit_comments.2015_01],
  [fh-bigquery:reddit_comments.2015_02],
  [fh-bigquery:reddit_comments.2015_03],
  [fh-bigquery:reddit_comments.2015_04],
  [fh-bigquery:reddit_comments.2015_05],
  [fh-bigquery:reddit_comments.2015_06],
  [fh-bigquery:reddit_comments.2015_07],
  [fh-bigquery:reddit_comments.2015_08],
  [fh-bigquery:reddit_comments.2015_09],
  [fh-bigquery:reddit_comments.2015_10],
  [fh-bigquery:reddit_comments.2015_11],
  [fh-bigquery:reddit_comments.2015_12],
	[fh-bigquery:reddit_comments.2016_01]
) x
where id IN (
	SELECT
		REGEXP_REPLACE(parent_id, 't1_',''), # strip 't1_' prefix 
	FROM
    [symmetric-curve-128220:IR.reactions_imgur] # This is the reactions table (output from reactions.sql)
	)