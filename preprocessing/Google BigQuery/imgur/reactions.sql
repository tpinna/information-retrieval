-- Reaction:
--     1) comment on a comment (prefixed with t1_ according to https://www.reddit.com/dev/api) 
--     2) considered good by community (score)
--     3) contains imgur link

SELECT
	*
FROM
	[fh-bigquery:reddit_comments.2007],
	[fh-bigquery:reddit_comments.2008],
	[fh-bigquery:reddit_comments.2009],
	[fh-bigquery:reddit_comments.2010],
	[fh-bigquery:reddit_comments.2011],
	[fh-bigquery:reddit_comments.2012],
	[fh-bigquery:reddit_comments.2013],
	[fh-bigquery:reddit_comments.2014],
	[fh-bigquery:reddit_comments.2015_01],
	[fh-bigquery:reddit_comments.2015_02],
	[fh-bigquery:reddit_comments.2015_03],
	[fh-bigquery:reddit_comments.2015_04],
	[fh-bigquery:reddit_comments.2015_05],
	[fh-bigquery:reddit_comments.2015_06],
	[fh-bigquery:reddit_comments.2015_07],
	[fh-bigquery:reddit_comments.2015_08],
	[fh-bigquery:reddit_comments.2015_09],
	[fh-bigquery:reddit_comments.2015_10],
	[fh-bigquery:reddit_comments.2015_11],
	[fh-bigquery:reddit_comments.2015_12],
	[fh-bigquery:reddit_comments.2016_01]
WHERE 
	parent_id LIKE 't1\x5f%' # (1)
	AND score > 0 # (2)
	AND body CONTAINS 'imgur.com/' # (3)