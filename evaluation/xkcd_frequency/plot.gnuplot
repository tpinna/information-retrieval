set terminal svg
set output "freq.svg"

unset key
unset xtics
set logscale y 10
set xlabel "XKCD"
set ylabel "Number of mentions"
plot 'xkcdfreq.dat' pointtype 7

