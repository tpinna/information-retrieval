#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Convenience wrapper for running relevantXKCD directly from source tree."""


from relevantXKCD.relevantXKCD import main


if __name__ == '__main__':
    main()
