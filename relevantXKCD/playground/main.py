import pyspark
from pyspark.sql import SQLContext
import load
#import index
#import loadindex


if __name__ == "__main__":

	# spark configurations

	appName = "Information retrieval"
	conf = pyspark.SparkConf().setAppName(appName).setMaster(master)
	sc = pyspark.SparkContext(conf=conf)
	sqlContext = SQLContext(sc)

	# load files

	filenames = ["./data/comments.json", "./data/reactions.json"]
	files = load.run(filenames, sc)

	# create index

	# read index

	# ir-systel
