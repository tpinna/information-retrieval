from random import shuffle
from copy import copy
from heapq import nlargest


import time


class myStopwatch:

    events = None

    class Events:
        last_added = []

        def __init__(self, name):
            self.name = name
            self.children = []
            self.index = {}
            self.start = time.time()
            self.end = None
            myStopwatch.Events.last_added.append(self)

        def add(self, event):
            if len(event) < 1:
                return
            elif len(event) == 1:
                e = event[0]
                assert e not in self.index  # Cannot add same event twice
                self.index[e] = len(self.children)
                self.children.append(myStopwatch.Events(e))
            else:
                # Cannot add subevent of nonexisting event
                assert event[0] in self.index
                self.children[self.index[event[0]]].add(event[1:])

        def stop(self):
            myStopwatch.Events.last_added.pop().end = time.time()

        def toString(self, lvl=0):
            try:
                delta = self.end - self.start
            except:  # self.end was None
                delta = ''
            s = str(delta) + "\t" * lvl + self.name + "\n"
            for child in self.children:
                s += child.toString(lvl + 1)
            return s

    def start(event):
        x = event.split('/')
        if not myStopwatch.events:
            myStopwatch.events = myStopwatch.Events('')
        myStopwatch.events.add(x)

    def stop():
        myStopwatch.events.stop()

    def getPretty():
        return myStopwatch.events.toString()

import random
l = [random.randint(0,100000) for i in range(100000)]
r = copy(l)

myStopwatch.start('sorted')
l10 = sorted(l, reverse=True)[:10]
myStopwatch.stop()

myStopwatch.start('nlargest')
r10 = nlargest(10, r)
myStopwatch.stop()

assert(r10 == l10)

print(myStopwatch.getPretty())