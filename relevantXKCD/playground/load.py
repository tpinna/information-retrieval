import json

def jsonLineToTuple(line):
	dicty = json.loads(line)
	return (dicty["link_id"], (dicty["body"], dicty["id"], dicty["parent_id"]))

def jsonLineToTupleReaction(line):
	dicty = json.loads(line)
	return (dicty["parent_id"], (dicty["body"], dicty["id"], dicty["link_id"]))

def readFiles(commentfilename, reactionfilename, sc):
	#read comments
	comments = sc.textfile(commentfilename)
	#convert to key, value
	comments = comments.map(jsonLineToTuple)
	#sort


	#read reactions
	reactions = sc.textfile(reactionfilename)
	#convert to key, value
	reactions.map(jsonLineToTupleReaction)

	#merge
	comments.join(reactions)

	return comments






def run(filenames, sc):

	#read files in memory
	comments = readFiles(filenames[0],filenames[1] , sc)
	


	