from . import zipfile2 as zipfile
from collections import Counter
from .tokenizer import tokenize_comment
import codecs
import hashlib
import json
import logging
import time

LOGGER = logging.getLogger(__name__)
_DECODER = codecs.getdecoder("utf-8")
DECODER = lambda x:_DECODER(x)[0]


def wrap(s, cols=75):
    r='\n'
    # This is a ceil, think about it. No math module imported.
    parts = -(-len(s)//cols)
    for i in range(parts):
        t = " \\" if i != parts-1 else ''
        r+= " > " + s[i*cols:i*cols+cols] + t + '\n'
    return r


class topK:
    def __init__(self,K):
        self.K = K
        self.topk = []  #stored in descending order of minscore
        self.minK = -1
        self.topkset = set()

    def update(self):
        self.topk.sort(key=lambda x: x.score, reverse=True)
        if self.topk != []:
            self.minK = self.topk[-1].score
    def ShouldBeTopK(self,doc):
        """
        checks if a new doc_id should be in topK
        """
        return len(self.topk) < self.K or doc.score > self.minK
    def insertInTopK(self, doc):
        """
        inserts a doc in topK: 
            - keeps the topk in descending order
            - if doc must be removed from topK, remove that doc
        """
        if self.__contains__(doc):
            return
        if not self.ShouldBeTopK(doc):
            return
        if len(self.topk)>=self.K:
            if self.topk[-1].score != self.minK:
                self.update()
            a = self.topk.pop()
            a.intopk = False
        self.topk.append(doc)
        doc.intopk = True
        self.update()
    def __contains__(self, item):
        return item.intopk



class Document:

    def __init__(self, doc_id):
        self.doc_id = doc_id
        self.score = 0
        self.intopk = False

    def __str__(self):
        return 'Document(\n' \
            '\tdoc_id = {}\n' \
            '\tscore = {}\n' \
            ')'.format(self.doc_id, str(self.score))

    def __repr__(self):
        return "<DOC("+self.doc_id+str(self.score)+")>"


class TermIndex:
    # if load complete = true: the entire termfile will be loaded into memory
    # else it will be loaded line per line
    load_complete = False
    def __init__(self, zipfiley):
        self.zippy = zipfiley

    @staticmethod
    def term_to_file(term):
        filename = hashlib.sha1(codecs.encode(term, "utf-8"))
        filename = str(filename.hexdigest()) + "_i.txt"
        return filename

    def init(self, termlist):
        te = [TermIndex.term_to_file(term) for term in termlist]
        self.myzip = zipfile.ZipFile(self.zippy, mode='r', to_extract=te)

    def getTerm(self, term):
        def readZipTerm():
            try:
                to_json = False
                filename = TermIndex.term_to_file(term)
                LOGGER.info((filename, term))
                with self.myzip.open(filename) as mytxt:
                    while True:
                        txt = mytxt.readline().rstrip()
                        if not txt:
                            break
                        txt = DECODER(txt)
                        yield json.loads(txt)
            except KeyError:
                pass
        def readZipTerm2():
            try:
                to_json = False
                filename = TermIndex.term_to_file(term)
                LOGGER.info((filename, term))
                with self.myzip.open(filename) as mytxt:
                    txts = mytxt.read()
                    txts = DECODER(txts)
                    txts = txts.split("\n")
                    yield json.loads(txts[0])
                    txts = map(json.loads, txts[1:])
                    for txt in txts:
                        yield txt
            except KeyError:
                pass
        if TermIndex.load_complete:
            return readZipTerm2()
        return readZipTerm()


class DocIndex:
    fast_all = False

    def __init__(self, zipfiley):
        self.zippy = zipfiley
        self.initialised = False

    def init(self, doclist=[]):
        if not self.initialised:
            if not self.fast_all:
                te = [doc.doc_id + "_i.txt" for doc in doclist]
                self.initialised = True
                self.myzip = zipfile.ZipFile(self.zippy, mode='r', to_extract=te)
            else:
                self.myzip = zipfile.ZipFile(self.zippy, mode='r')

    def getDoc(self, doc_id):
        """Get document metadata.
        
        Returns a list with four elements:
            [0] comment_id
            [1] term_list -- a list of [term, score]-pairs
            [2] comment_body
            [3] url_list -- a list of [url, score]-pairs

        Call it as follows:
            [comment_id, term_list, comment_body, url_list] = getDoc(doc_id)
        """
        try:
            filename = doc_id + "_i.txt"
            with self.myzip.open(filename) as mytxt:
                txt = mytxt.read()
                txt = DECODER(txt)
                return json.loads(txt)
        except KeyError:
            return None

    def getRaDocScore(self, doc, terms=[]):
        """
        Do a random access on disk and get docpart of the score for a certain set of terms
        """
        LOGGER.info("random access")
        js = None
        if not DocIndex.fast_all:
            di = DocIndex(self.zippy)
            di.init([doc])
            js = di.getDoc(doc.doc_id)
        else:
            # if fast_all, load all offsets into memory. this takes a second the first time
            #    but increases performance for the following random accesses
            # if we could predict the running time somehow, we could make this choice dynamically, which would be extremely cool
            self.init()
            js = self.getDoc(doc.doc_id)
        js = {t[0]:t[1] for t in js[1]}
        return sum( (js[term] for term in terms if term in js) )


    def willGetDocs(self, docs):
        self.di = DocIndex(self.zippy)
        self.di.init(docs)


    def getRaDocScore2(self, doc, terms=[]):
        """
        Do a random access on disk and get docpart of the score for a certain set of terms
        """
        LOGGER.info("random access2")
        js = self.di.getDoc(doc.doc_id)
        js = {t[0]:t[1] for t in js[1]}
        return sum( (js[term] for term in terms if term in js) )


class RetrievalSystem:

    def __init__(self, term_index_path: str, doc_index_path: str):
        LOGGER.debug("Initializing Retrieval System: Threshold Algorithm")
        self.term_index = TermIndex(term_index_path)
        self.doc_index = DocIndex(doc_index_path)

    def retrieve(self, query: str, idf_pruning_absolute_cutoff: float,
                 idf_pruning_min_percentage: float, time_cutoff: float,
                 champion_lists_length_multiplier: float):
        LOGGER.debug("retrieve: '%s'" % query)

        # List of terms (with duplicates). Looks like ['term', ... ]
        terms = [x[0][0] for x in tokenize_comment(
            json.dumps({"id": None, "body": query}))]

        # term frequency in query
        tfq = Counter(terms)

        # List of terms (no duplicates). Looks like ['term', ... ]
        terms = list(set(terms))

        if len(terms) == 0:
            return []

        k3 = 1.6  # between 1.2 and 2

        query_terms_list = [(term, (k3 + 1) * tfq / (k3 + tfq))
                            for term, tfq in tfq.items()]
        query_terms_list.sort(key=lambda x: x[0])

        # K from top-K
        K = 5
        topk_ = topK(K)

        # Get the posting lists corresponding to the query terms. Looks like
        # [[(doc,score), ...], ... ]
        self.term_index.init(terms)
        plists = [self.term_index.getTerm(term) for term in terms]

        #idf pruning
        #parameters
        absolute_cutoff = idf_pruning_absolute_cutoff
        min_percentage = idf_pruning_min_percentage
        # get idf values
        term_to_idf = []
        for xx in range(len(plists)):
            x = plists[xx]
            term,idf = next(x, (terms[xx], 0))
            term_to_idf.append((term,idf))
        term_to_idf = dict(term_to_idf)
        if idf_pruning_absolute_cutoff > 0 and idf_pruning_min_percentage < 1:
            #sort according to idf
            order = list(range(len(terms)))
            order.sort(key=lambda x: term_to_idf[ terms[x] ], reverse=True)
            terms = [terms[i] for i in order]
            plists = [plists[i] for i in order]
            #prune
            ii = len(terms)-1
            for i in range(len(terms)):
                term = terms[i]
                if term_to_idf[term] < absolute_cutoff and i/len(terms) > min_percentage:
                    ii=i
                    break
            terms = terms[:ii]
            plists = plists[:ii]
            LOGGER.info("remaining terms: " +str(terms))

        #championlist cutoff
        championlistfactor = champion_lists_length_multiplier
        champs=[]

        # load full champion lists
        for i in range(len(plists)):
            generator = plists[i]
            max_len = int(championlistfactor*term_to_idf[terms[i]])
            plist = []
            doc_id, s_td = next(generator, (None, 0))
            while doc_id and len(plist) < max_len:
                plist.append([doc_id, s_td])
                doc_id, s_td = next(generator, (None, 0))
            plists[i] = plist
            champs.append(min(max_len, len(plist)))

        high = [0] * len(terms)
        sum_high = 0
        depth = -1

        # if docindex is false, a random access costs about 0.07 seconds
        # if it's true, a random access will happen quite fast (on ssd), but the initial cost will be 1 second
        DocIndex.fast_all = False 

        LOGGER.debug(list(zip(['absolute_cutoff', 'min_percentage', 'time_cutoff'], [absolute_cutoff, min_percentage, time_cutoff])))

        time0 = time.time()
        while True:
            depth += 1
            done = True
            for i in range(0, len(terms)):
                if depth < champs[i]:
                    done = False
                    doc_id, s_td = plists[i][depth]
                    sum_high -= high[i]
                    high[i] = s_td
                    sum_high += high[i]
                    if doc_id:
                        doc = Document(doc_id)
                        doc.score = s_td
                        for j in range(0, len(terms)):
                            if i != j:  # find posting with doc_id in plists[j][depth:]
                                for k in range(depth, champs[j]):
                                    if plists[j][k][0] == doc_id:
                                        plists[j][k][0] = ''
                                        doc.score += plists[j][k][1]
                        topk_.insertInTopK(doc)
                elif depth == champs[i]:
                    LOGGER.debug(terms[i] + " ended at loop "+ str(depth))
                    sum_high -= high[i]
                    high[i] = 0
            if done:
                break
            if sum_high > topk_.minK:
                break
            if time_cutoff:
                if time.time() - time0 > time_cutoff:
                    break

        LOGGER.info("there were "+str(depth)+" loops needed for finding topk")

        top = []

        # compute scores for top-K
        self.doc_index.init(topk_.topk)
        LOGGER.debug('Top {} Documents'.format(K))

        for doc in topk_.topk:
            [doc_id, doc_terms_list, doc_body, url_list] =\
                self.doc_index.getDoc(doc.doc_id)
            doc_score = RetrievalSystem.__score(doc_terms_list,
                                                query_terms_list)
            LOGGER.debug("{:10.3f} - {} - {}\ncomment: {}"\
                .format(doc_score, doc_id, url_list, wrap(repr(doc_body))))
            for xkcd_url, xkcd_score in url_list:
                top.append((xkcd_url, (doc_score, xkcd_score)))
        top.sort(key=lambda x: x[1], reverse=True)

        return top

    @staticmethod
    def __score(doc_terms_list, query_terms_list):
        """Compute document score.

            Arguments: two lists of (term, score)-pairs
                       sorted by term in ascending order
        """
        a, b = iter(doc_terms_list), iter(query_terms_list)
        i, j = next(a, None), next(b, None)
        score = 0
        while (i and j):
            if (i[0] == j[0]):
                score += i[1] * j[1]
                i, j = next(a, None), next(b, None)
            elif (i[0] < j[0]):
                i = next(a, None)
            else:
                j = next(b, None)
        return score
