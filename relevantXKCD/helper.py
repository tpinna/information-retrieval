import time


class myStopwatch:

    events = None

    class Events:
        last_added = []

        def __init__(self, name):
            self.name = name
            self.children = []
            self.index = {}
            self.start = time.time()
            self.end = None
            myStopwatch.Events.last_added.append(self)

        def add(self, event):
            if len(event) < 1:
                return
            elif len(event) == 1:
                e = event[0]
                assert e not in self.index  # Cannot add same event twice
                self.index[e] = len(self.children)
                self.children.append(myStopwatch.Events(e))
            else:
                # Cannot add subevent of nonexisting event
                assert event[0] in self.index
                self.children[self.index[event[0]]].add(event[1:])

        def stop(self):
            myStopwatch.Events.last_added.pop().end = time.time()

        def toString(self, lvl=0):
            try:
                delta = self.end - self.start
            except:  # self.end was None
                delta = ''
            s = str(delta) + "\t" * lvl + self.name + "\n"
            for child in self.children:
                s += child.toString(lvl + 1)
            return s

    def start(event):
        x = event.split('/')
        if not myStopwatch.events:
            myStopwatch.events = myStopwatch.Events('')
        myStopwatch.events.add(x)

    def stop():
        myStopwatch.events.stop()

    def getPretty():
        return myStopwatch.events.toString()

# TODO: fix problem: sometimes you cannot iterate multiple times trough a yielder
# solution1: store every inbetween step in a list ( memory usage :( )
# solution2: use iterator.tee to copy an iterator ( again, memory usage )
# solution3: create yieldfunction generators


class Functional:

    def __init__(self, iterable):
        self.iterable = iterable

    def __iter__(self):
        return iter(self.iterable)

    def flatMap(self, function):
        def flatmapper():
            for element in self:
                for result in function(element):
                    yield result
        return Functional(flatmapper())

    # def mapPartitions(self, function):
    # def mapPartitionsWithIndex(self, function)
    # def mapPartitionsWithSplit(self, function)
    # def getNumPartitions(self)

    def filter(self, function):
        def filterer():
            for element in self:
                if function(element):
                    yield element
        return Functional(filterer())

    def distinct(self):
        def distincter():
            cur = None
            for element in self.sort:
                if element != cur:
                    yield element
                    cur = element
        return Functional(distincter())

    def takeSample(self, withreplacement, num, seed):
        # TODO
        return

    def sort(self):
        return Functional(sorted(list(self)))

    def sortByKey(self):
        return Functional(sorted(list(self)))

    def groupByKey(self):
        def grouper():
            curkey = None
            curlist = []
            first = True
            for element in self.sortByKey():
                if curkey == element[0]:
                    curlist.append(element[1])
                else:
                    if first:
                        first = False
                    else:
                        yield (curkey, curlist)
                    curkey = element[0]
                    curlist = [element[1]]
            yield (curkey, curlist)
        return Functional(grouper())

    def reduceByKey(self, operator, init=0):
        def reducer():
            for element in self.groupByKey():
                rvalue = init
                for value in element[1]:
                    rvalue = operator(rvalue, value)
                yield (element[0], rvalue)
        return Functional(reducer())

    def map(self, mapfunction):
        def mapper():
            for element in self:
                yield mapfunction(element)
        return Functional(mapper())

    def mapValues(self, mapfunction):
        def valuemapper():
            for element in self:
                yield (element[0], mapfunction(element[1]))
        return Functional(valuemapper())

    def cache(self):
        return Functional(list(self))

    def collect(self):
        return list(self)

    def intersection(self, iterable):
        a = self.sort()
        b = Functional(iterable).sort()
        i, j = next(a), next(b)
        while (i and j):
            if (i == j):
                # compute
                i, j = next(a), next(b)
            elif (i < j):
                i = next(a)
            else:
                j = next(b)


if __name__ == "__main__":
    testlist = [('a', 1), ('b', 1), ('a', 1)]
    f = Functional(testlist)
    print(f.sortByKey().collect())
    print(f.groupByKey().collect())
    print(f.reduceByKey(lambda x, y: x + y).collect())
    print(f.map(lambda x: x[0] + str(x[1])).collect())
    print(f.mapValues(lambda x: 2 * x).collect())
