# -*- coding: utf-8 -*-

"""relevantXKCD.relevantXKCD: provides entry point main()."""

from collections import namedtuple
from .helper import myStopwatch
from .indexer import IndexerMapReduceJob
import argparse
import logging
import os
import shutil
import sys

LOGGER = logging.getLogger(__file__)
DIR = os.path.dirname(__file__)

full_help = ""


def import_alg(name):
    global RetrievalSystem
    if name == 'ta':
        from .retriever2 import RetrievalSystem
    elif name == 'nra':
        from .retriever import RetrievalSystem


def make_full_help(parser):
    # print main help
    parts = []
    parts.append('{:-^80}\n'.format(' {} '.format(parser.prog)))
    parts[-1] += parser.format_help()

    # retrieve subparsers from parser
    subparsers_actions = [
        action for action in parser._actions 
        if isinstance(action, argparse._SubParsersAction)]
    # there will probably only be one subparser_action,
    # but better save than sorry
    for subparsers_action in subparsers_actions:
        # get all subparsers and print help
        for choice, subparser in subparsers_action.choices.items():
            if (choice in ['build', 'search']):
                parts.append('{:-^80}\n'.format(' {} '.format(choice)))
                parts[-1] += subparser.format_help()
    return '\n'.join(parts)


def parse_args():
    """Return an object with parsed command line arguments as its attributes.
    """

    # Proper Python command line parsing
    parser = argparse.ArgumentParser(prog='xkcd',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # Named argument (optional)
    parser.add_argument('-log_level', default="WARNING",
        choices=["NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="logging level")

    # Positional argument (mandatory)

    subparsers = parser.add_subparsers(dest="command")
    # subparsers.required=True

    # command="build"
    parser_build = subparsers.add_parser("build", help="build index",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_build.add_argument("-comments", default=DIR+"/xkcd/comments.json",
        type=str, help="input comments json files path (supports wildcards)")
    parser_build.add_argument("-reactions", default=DIR+"/xkcd/reactions.json",
        type=str, help="input reactions json files path (supports wildcards)")
    parser_build.add_argument("-output", default=DIR+"/index",
        type=str, help="index will be placed in this directory. WARNING: make"
        " sure you remove previously generated index files (if any) from this"
        " directory before starting a build")
    parser_build.add_argument("-threads", default=8, type=int,
                              help="number of threads in local mode")

    # command="search"
    parser_search = subparsers.add_parser("search",
        help="load index and search for query",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_search.add_argument("-drop", default="0", type=float,
        help="drop term t from the query if idf(t) < X (see -keep)")
    parser_search.add_argument("-keep", default="1", type=float,
        help="never drop more than this fraction of query terms (see -drop)")
    parser_search.add_argument("-time", default="0", type=float,
        help="stop searching if it takes longer than X seconds. "
             "WARNING: specified time may be exceeded")
    parser_search.add_argument("-precision", default="1000", type=float,
        help="consider at most int(idf(t)*precision) postings for term t. "
             "(aka champion lists)")
    parser_search.add_argument("-input", default=DIR+"/index",
        help="dir where 'index_term.zip' and 'index_doc.zip' are located")
    parser_search.add_argument("-algorithm", default='nra', type=str,
        choices=["ta", "nra"], help="ta -- Threshold Algorithm, "
                                    "nra -- No-Random-Access Algorithm")

    parser_search.add_argument("query", nargs='?',
        default="I think it's a fun programming language",
        help="Your search query")

    # command in ["searchtest", "build_xkcd", "build_smallxkcd"]
    subparsers.add_parser("searchtest", help="execute some example searches")
    subparsers.add_parser("build_xkcd",
                          help="build index with full xkcd data set")
    subparsers.add_parser("build_smallxkcd",
                          help="build index with reduced xkcd data set")

    global full_help
    full_help = make_full_help(parser)

    return parser.parse_args()


def main():
    myStopwatch.start("parse args")
    args = parse_args()
    myStopwatch.stop()

    logging.basicConfig(level=getattr(logging, args.log_level))

    if args.command in ["build", "build_xkcd", "build_smallxkcd"]:

        if args.command == "build_xkcd":
            if os.path.isdir(DIR+"/index"):
                shutil.rmtree(DIR+"/index")
            indexer = IndexerMapReduceJob(DIR+"/xkcd/comments.json",
                DIR+"/xkcd/reactions.json", DIR+"/index", 8)
        elif args.command == "build_smallxkcd":
            if os.path.isdir(DIR+"/index"):
                shutil.rmtree(DIR+"/index")
            indexer = IndexerMapReduceJob(
                DIR+"/playground/comments_sample.json",
                DIR+"/playground/reactions_sample.json", DIR+"/index", 8)
        else:
            if os.path.isdir(args.output):
                print("WARNING: {} exists. Make sure you remove previously "
                      "generated index files (if any) from this directory "
                      "before starting a build.".format(args.output),
                      file=sys.stderr)
            indexer = IndexerMapReduceJob(args.comments,
                args.reactions, args.output, args.threads)

        myStopwatch.start("indexer.run()")
        indexer.run()
        myStopwatch.stop()

        LOGGER.info("TIMINGS:\n" + myStopwatch.getPretty())
    elif args.command in ["search", "searchtest"]:
        import_alg(args.algorithm)
        
        if args.command == "searchtest":
            Args = namedtuple('Args', ['input', 'query', 'drop', 'keep', 'time', 'precision'])
            args = Args(DIR+'/index', "I think it's a fun programming language", 0, 1, 0, 1000)

        input_path = (args.input + '/index_term.zip',
                      args.input + '/index_doc.zip')

        LOGGER.debug("Looking for '%s' and '%s'" % input_path)

        myStopwatch.start("setup retrieval")
        rs = RetrievalSystem(*input_path)
        myStopwatch.stop()

        LOGGER.debug("term_index: %s" % str(rs.term_index))

        myStopwatch.start("running retrieval")
        
        import cProfile, pstats, io
        pr = cProfile.Profile()
        pr.enable()

        r = rs.retrieve(args.query, args.drop, args.keep, args.time, args.precision)

        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())

        myStopwatch.stop()

        print('{:16}  {:12}    {}'.format('similarity_score', 'reddit_score', 'xkcd_url'))
        [print('{:16.3f}  {:12}    {}'.format(score[0], score[1], xkcd_url))
         for xkcd_url, score in r]

        LOGGER.info("TIMINGS:\n" + myStopwatch.getPretty())
    else:
        print(full_help)

