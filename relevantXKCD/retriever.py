from . import zipfile2 as zipfile
from collections import Counter
from .tokenizer import tokenize_comment
import codecs
import hashlib
import json
import logging
from functools import lru_cache
import time

LOGGER = logging.getLogger(__name__)
_DECODER = codecs.getdecoder("utf-8")
DECODER = lambda x:_DECODER(x)[0]


def wrap(s, cols=75):
    r='\n'
    # This is a ceil, think about it. No math module imported.
    parts = -(-len(s)//cols)
    for i in range(parts):
        t = " \\" if i != parts-1 else ''
        r+= " > " + s[i*cols:i*cols+cols] + t + '\n'
    return r


class topK:
    def __init__(self,K):
        self.K = K
        self.topk = []  #stored in descending order of minscore
        self.minK = -1
        self.topkset = set()

    def update(self):
        self.topk.sort(key=lambda x: x.worst_score, reverse=True)
        if self.topk != []:
            self.minK = self.topk[-1].worst_score
    def ShouldBeTopK(self,doc):
        """
        checks if a new doc_id should be in topK
        """
        return len(self.topk) < self.K or doc.worst_score > self.minK
    def isCandidate(self, doc):
        """
        checks if a document is a candidate for topK
        """
        return doc.best_score > self.minK 
    def insertInTopK(self, doc):
        """
        inserts a doc in topK: 
            - keeps the topk in descending order
            - if doc must be removed from topK, remove that doc
        """
        if self.__contains__(doc):
            return
        if not self.ShouldBeTopK(doc):
            return
        if len(self.topk)>=self.K:
            if self.topk[-1].worst_score != self.minK:
                self.update()
            a = self.topk.pop()
            a.intopk = False
        self.topk.append(doc)
        doc.intopk = True
        self.update()
    def __contains__(self, item):
        return item.intopk



class Document:

    def __init__(self, doc_id, term=None):
        self.doc_id = doc_id
        self.worst_score = 0
        self.best_score = 0
        if term:
            self.accounted_terms = frozenset([term])
        else:
            self.accounted_terms = frozenset()
        self.intopk = False

    def key(self):
        return self.worst_score, self.best_score

    def add_accounted(self, term):
        self.accounted_terms = self.accounted_terms | frozenset([term])

    def __str__(self):
        return 'Document(\n' \
            '\tdoc_id = {}\n' \
            '\tworst_score = {}\n' \
            '\tbest_score = {}\n' \
            '\taccounted_terms = {}\n' \
            ')'.format(self.doc_id, str(self.worst_score),
                       str(self.best_score), str(self.accounted_terms))
    def __repr__(self):
        return "<DOC("+self.doc_id+str((self.worst_score, self.best_score))+")>"


class TermIndex:
    # if load complete = true: the entire termfile will be loaded into memory
    # else it will be loaded line per line
    load_complete = False
    def __init__(self, zipfiley):
        self.zippy = zipfiley

    @staticmethod
    def term_to_file(term):
        filename = hashlib.sha1(codecs.encode(term, "utf-8"))
        filename = str(filename.hexdigest()) + "_i.txt"
        return filename

    def init(self, termlist):
        te = [TermIndex.term_to_file(term) for term in termlist]
        self.myzip = zipfile.ZipFile(self.zippy, mode='r', to_extract=te)

    def getTerm(self, term):
        def readZipTerm():
            try:
                to_json = False
                filename = TermIndex.term_to_file(term)
                LOGGER.info((filename, term))
                with self.myzip.open(filename) as mytxt:
                    while True:
                        txt = mytxt.readline().rstrip()
                        if not txt:
                            break
                        txt = DECODER(txt)
                        yield json.loads(txt)
            except KeyError:
                pass
        def readZipTerm2():
            try:
                to_json = False
                filename = TermIndex.term_to_file(term)
                LOGGER.info((filename, term))
                with self.myzip.open(filename) as mytxt:
                    txts = mytxt.read()
                    txts = DECODER(txts)
                    txts = txts.split("\n")
                    yield json.loads(txts[0])
                    txts = map(json.loads, txts[1:])
                    for txt in txts:
                        yield txt
            except KeyError:
                pass
        if TermIndex.load_complete:
            return readZipTerm2()
        return readZipTerm()


class DocIndex:
    fast_all = False

    def __init__(self, zipfiley):
        self.zippy = zipfiley
        self.initialised = False

    def init(self, doclist=[]):
        if not self.initialised:
            if not self.fast_all:
                te = [doc.doc_id + "_i.txt" for doc in doclist]
                self.initialised = True
                self.myzip = zipfile.ZipFile(self.zippy, mode='r', to_extract=te)
            else:
                self.myzip = zipfile.ZipFile(self.zippy, mode='r')

    def getDoc(self, doc_id):
        """Get document metadata.
        
        Returns a list with four elements:
            [0] comment_id
            [1] term_list -- a list of [term, score]-pairs
            [2] comment_body
            [3] url_list -- a list of [url, score]-pairs

        Call it as follows:
            [comment_id, term_list, comment_body, url_list] = getDoc(doc_id)
        """
        try:
            filename = doc_id + "_i.txt"
            with self.myzip.open(filename) as mytxt:
                txt = mytxt.read()
                txt = DECODER(txt)
                return json.loads(txt)
        except KeyError:
            return None

    def getRaDocScore(self, doc, terms=[]):
        """
        Do a random access on disk and get docpart of the score for a certain set of terms
        """
        LOGGER.info("random access")
        js = None
        if not DocIndex.fast_all:
            di = DocIndex(self.zippy)
            di.init([doc])
            js = di.getDoc(doc.doc_id)
        else:
            # if fast_all, load all offsets into memory. this takes a second the first time
            #    but increases performance for the following random accesses
            # if we could predict the running time somehow, we could make this choice dynamically, which would be extremely cool
            self.init()
            js = self.getDoc(doc.doc_id)
        js = {t[0]:t[1] for t in js[1]}
        return sum( (js[term] for term in terms if term in js) )


    def willGetDocs(self, docs):
        self.di = DocIndex(self.zippy)
        self.di.init(docs)


    def getRaDocScore2(self, doc, terms=[]):
        """
        Do a random access on disk and get docpart of the score for a certain set of terms
        """
        LOGGER.info("random access2")
        js = self.di.getDoc(doc.doc_id)
        js = {t[0]:t[1] for t in js[1]}
        return sum( (js[term] for term in terms if term in js) )


class RetrievalSystem:

    def __init__(self, term_index_path: str, doc_index_path: str):
        LOGGER.debug("Initializing Retrieval System: No-Random-Access Algorithm")
        self.term_index = TermIndex(term_index_path)
        self.doc_index = DocIndex(doc_index_path)

    def retrieve(self, query: str, idf_pruning_absolute_cutoff: float,
                 idf_pruning_min_percentage: float, time_cutoff: float,
                 champion_lists_length_multiplier: float):
        LOGGER.debug("retrieve: '%s'" % query)

        # List of terms (with duplicates). Looks like ['term', ... ]
        terms = [x[0][0] for x in tokenize_comment(
            json.dumps({"id": None, "body": query}))]

        # term frequency in query
        tfq = Counter(terms)

        # List of terms (no duplicates). Looks like ['term', ... ]
        terms = list(set(terms))

        if len(terms) == 0:
            return []

        k3 = 1.6  # between 1.2 and 2

        query_terms_list = [(term, (k3 + 1) * tfq / (k3 + tfq))
                            for term, tfq in tfq.items()]
        query_terms_list.sort(key=lambda x: x[0])

        # K from top-K
        K = 5
        topk_ = topK(K)

        # looks like {doc_id : doc}
        doc_index = {}

        # Get the posting lists corresponding to the query terms. Looks like
        # [[(doc,score), ...], ... ]
        self.term_index.init(terms)
        plists = [self.term_index.getTerm(term) for term in terms]

        #idf pruning
        #parameters
        absolute_cutoff = idf_pruning_absolute_cutoff
        min_percentage = idf_pruning_min_percentage
        # get idf values
        term_to_idf = []
        for xx in range(len(plists)):
            x = plists[xx]
            term,idf = next(x, (terms[xx], 0))
            term_to_idf.append((term,idf))
        term_to_idf = dict(term_to_idf)
        if idf_pruning_absolute_cutoff > 0 and idf_pruning_min_percentage < 1:
            #sort according to idf
            order = list(range(len(terms)))
            order.sort(key=lambda x: term_to_idf[ terms[x] ], reverse=True)
            terms = [terms[i] for i in order]
            plists = [plists[i] for i in order]
            #prune
            ii = len(terms)-1
            for i in range(len(terms)):
                term = terms[i]
                if term_to_idf[term] < absolute_cutoff and i/len(terms) > min_percentage:
                    ii=i
                    break
            terms = terms[:ii]
            plists = plists[:ii]
            LOGGER.info("remaining terms: " +str(terms))

        #championlist cutoff
        championlistfactor = champion_lists_length_multiplier
        champs=[int(championlistfactor*term_to_idf[term]) for term in terms]

        all_terms = frozenset(range(0, len(terms)))
        high = [0] * len(terms)
        sum_high = 0
        depth = -1

        @lru_cache(maxsize=128)
        def not_accounted_terms(b):
            return all_terms - b
        
        # if docindex is false, a random access costs about 0.07 seconds
        # if it's true, a random access will happen quite fast (on ssd), but the initial cost will be 1 second
        DocIndex.fast_all = False 

        LOGGER.debug(list(zip(['absolute_cutoff', 'min_percentage', 'time_cutoff'], [absolute_cutoff, min_percentage, time_cutoff])))

        deleted_docs = set()
        time0 = time.time()
        while True:
            done = True
            # scan: 
            #   - get the new documents
            #   - add document to {doc_id : doc} (wrongfully called doc_index)
            #   - update worst score with newly found score
            #   - save high[i], which is the current score for maximum
            for _ in range(20): 
                depth += 1 # it was incorrect to not include in this loop
                for i in range(0, len(terms)):
                    if depth < champs[i]:
                        doc_id, s_td = next(plists[i], (None, 0))
                        if doc_id is not None:
                            done = False
                            sum_high -= high[i]
                            high[i] = s_td
                            sum_high += high[i]
                            if doc_id not in doc_index:
                                if sum_high > topk_.minK and not doc_id in deleted_docs: #if new doc, then sum(high) already is an upperbound
                                    doc = Document(doc_id, i)
                                    doc_index[doc_id] = doc
                                    doc.worst_score += s_td
                                    topk_.insertInTopK(doc)
                                else:
                                    deleted_docs.add(doc_id)
                            else:
                                doc = doc_index[doc_id]
                                doc.add_accounted(i)
                                prevscore = None
                                doc.worst_score += s_td
                                if doc in topk_ and prevscore == topk_.minK:
                                    topk_.update() # there's actually only need for update if minK is affected
                                topk_.insertInTopK(doc)
                        else:
                            champs[i] = depth
                            LOGGER.debug(terms[i] + " ended at loop " + str(depth))
                            sum_high -= high[i]
                            high[i] = 0
                    elif depth == champs[i]:
                        LOGGER.debug(terms[i] + " ended at loop " + str(depth))
                        sum_high -= high[i]
                        high[i] = 0
            if done:
                break
            if time_cutoff:
                if time.time() - time0 > time_cutoff:
                    break
            #prune docs with a high score that's not high enough with the calculated high score
            if len(doc_index) > K:
                for doc_id, doc in list(doc_index.items()):
                    doc.best_score = doc.worst_score
                    for i in not_accounted_terms(doc.accounted_terms):
                        doc.best_score += high[i]
                    if not topk_.isCandidate(doc):
                        del doc_index[doc.doc_id]
                        deleted_docs.add(doc.doc_id)
                if len(doc_index) == K:
                    break

        LOGGER.info("there were "+str(depth)+" loops needed for finding topk")

        top = []

        # compute scores for top-K
        self.doc_index.init(topk_.topk)
        LOGGER.debug('Top {} Documents'.format(K))

        for doc in topk_.topk:
            [doc_id, doc_terms_list, doc_body, url_list] =\
                self.doc_index.getDoc(doc.doc_id)
            doc_score = RetrievalSystem.__score(doc_terms_list,
                                                query_terms_list)
            LOGGER.debug("{:10.3f} - {} - {}\ncomment: {}"\
                .format(doc_score, doc_id, url_list, wrap(repr(doc_body))))
            for xkcd_url, xkcd_score in url_list:
                top.append((xkcd_url, (doc_score, xkcd_score)))
        top.sort(key=lambda x: x[1], reverse=True)

        return top

    @staticmethod
    def __score(doc_terms_list, query_terms_list):
        """Compute document score.

            Arguments: two lists of (term, score)-pairs
                       sorted by term in ascending order
        """
        a, b = iter(doc_terms_list), iter(query_terms_list)
        i, j = next(a, None), next(b, None)
        score = 0
        while (i and j):
            if (i[0] == j[0]):
                score += i[1] * j[1]
                i, j = next(a, None), next(b, None)
            elif (i[0] < j[0]):
                i = next(a, None)
            else:
                j = next(b, None)
        return score


if __name__ == "__main__":
    """import time
    # time0 = time.time()
    di = DocIndex("index/index_doc.zip")
    # di.getRaDocScore(Document("c72jmv7"), ["chemistri", "biolog", "boerka"])
    # print(time.time() -time0)

    topk = [Document(x) for x in ["cgiw9g4", "ccqriyo", "cs2ovi3", "cqqlgxf", "c8x2ehp"]]
    terms = ["of", "my", "stori", "life"]

    scores1 = []
    time0 = time.time()
    for doc in topk:
        scores1.append(di.getRaDocScore(doc, terms))
    print(time.time() -time0)

    scores2 = []
    time0 = time.time()
    di.willGetDocs(topk)
    for doc in topk:
        scores2.append(di.getRaDocScore2(doc, terms))
    print(time.time() -time0)

    assert(scores1 == scores2)
    assert(scores1 is not scores2)"""
    #test topK
    
    #test building
    topk = topK(5)
    for i in range(5):
        a = Document(str(i))
        a.worst_score = i
        print(topk.insertInTopK(a) == None)
    print(topk.topk)
    topk.update()
    #test inserting at end
    i = 0.5
    a = Document(str(i))
    a.worst_score = i
    print(topk.insertInTopK(a).__repr__())
    print(topk.topk)

    #test inserting at middle
    i = 2.5
    a = Document(str(i))
    a.worst_score = i
    print(topk.insertInTopK(a).__repr__())
    print(topk.topk)

    #test inserting at begin
    i = 4.5
    a = Document(str(i))
    a.worst_score = i
    print(topk.insertInTopK(a).__repr__())
    print(topk.topk)


