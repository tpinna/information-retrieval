# -*- coding: utf-8 -*-

"""relevantXKCD.relevantXKCD: provides entry point main()."""

from .retriever import RetrievalSystem
import argparse
import logging
import os

LOGGER = logging.getLogger(__file__)
DIR = os.path.dirname(__file__)


def import_alg(name):
    global RetrievalSystem
    if name == 'ta':
        from .retriever2 import RetrievalSystem
    elif name == 'nra':
        from .retriever import RetrievalSystem


def parse_args():
    """Return an object with parsed command line arguments as its attributes.
    """

    # Proper Python command line parsing
    parser = argparse.ArgumentParser(prog="xkcd",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # Named argument (optional)
    parser.add_argument('-log_level', default="WARNING",
        choices=["NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="logging level")

    parser.add_argument("-drop", default="0", type=float,
        help="drop term t from the query if idf(t) < X (see -keep)")
    parser.add_argument("-keep", default="1", type=float,
        help="never drop more than this fraction of query terms (see -drop)")
    parser.add_argument("-time", default="0", type=float,
        help="stop searching if it takes longer than X seconds. "
             "Warning: specified time may be exceeded.")
    parser.add_argument("-precision", default="1000", type=float,
        help="consider at most int(idf(t)*precision) postings for term t. "
             "(aka champion lists)")
    parser.add_argument("-input", default=DIR+"/index",
        help="dir where 'index_term.zip' and 'index_doc.zip' are located")
    parser.add_argument("-algorithm", default='nra', type=str,
        choices=["ta", "nra"], help="ta -- Threshold Algorithm, "
                                    "nra -- No-Random-Access Algorithm.")

    parser.add_argument('query', help="Your search query")

    return parser.parse_args()


def main():
    args = parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level))

    import_alg(args.algorithm)

    input_path = (args.input + '/index_term.zip',
                  args.input + '/index_doc.zip')

    LOGGER.debug("Looking for '%s' and '%s'" % input_path)

    rs = RetrievalSystem(*input_path)

    LOGGER.debug("term_index: %s" % str(rs.term_index))

    r = rs.retrieve(args.query, args.drop, args.keep, args.time,
                    args.precision)

    [print('{:16.3f}  {:12}    {}'.format(score[0], score[1], xkcd_url))
     for xkcd_url, score in r]
