from . import zipfile2 as zipfile
from .helper import myStopwatch
from .tokenizer import tokenize_comment
import codecs
import hashlib
import json
import logging
import math
import operator
import os
import re

LOGGER = logging.getLogger(__name__)


class IndexerMapReduceJob:

    def __init__(self, inPathComments: str, inPathReactions: str, outPath: str,
                 nparallel: int):
        self.inPathComments = inPathComments
        self.inPathReactions = inPathReactions
        self.outPath = outPath
        self.nparallel = nparallel

    def run(self):
        """ Run indexer map reduce job and store output in outPath
        """
        # see http://spark.apache.org/docs/latest/programming-guide.html
        import pyspark
        conf = pyspark.SparkConf()\
            .setAppName("InvertedIndex")\
            .setMaster("local[%d]" % self.nparallel)\
            .set("spark.executor.memory", "1g")

        sc = pyspark.SparkContext(conf=conf)

        # preprocessing

        # looks like [((term, docID, doclen ), 1), ...]
        myStopwatch.start("indexer.run()/terms")
        comments = sc.textFile(self.inPathComments, use_unicode=True,
                               minPartitions=self.nparallel).persist(
            pyspark.StorageLevel(True, True, False, False, 1))
        terms = comments.flatMap(tokenize_comment)\
            .persist(pyspark.StorageLevel(True, True, False, False, 1))
        myStopwatch.stop()

        # Looks like [(term,df),...]
        df = terms.distinct()\
            .map(lambda x: (x[0][0], 1))\
            .reduceByKey(operator.add).cache()
        # looks like [(term, (df,[(docID,tf, doclength),...] ), ...), ...]
        tfdf = terms.reduceByKey(operator.add)\
            .map(lambda x : (x[0][0], (x[0][1], x[1], x[0][2])) )\
            .groupByKey()\
            .join(df).cache()
        # looks like (total number of terms, total number of documents)
        stats = terms.map(lambda x: (x[0][1], x[0][2], 1))\
            .distinct()\
            .map(lambda x: (x[1], x[2]))\
            .reduce(lambda x, y: (x[0] + y[0], x[1] + y[1]))  # \

        # create term index

        k_1 = 1.6
        N = stats[1]
        L_avg = stats[0] / stats[1]
        b = 0.75
        minscore = 0

        def update(x):
            term, postings_df = x
            postings = postings_df[0]
            df_t = postings_df[1]
            idf = math.log(N/df_t)
            def iterable():
                for doc_id, tf_td, L_d in postings:
                    score = idf*(k_1+1)*tf_td\
                            /(k_1*((1-b) + b*(L_d/L_avg)) + tf_td)
                    if score > minscore:
                        p = (doc_id, score)
                        yield p
            listy = list(iterable())
            return (len(listy),
                    ((term, idf) , sorted(listy, key=lambda x: x[1], reverse=True)))

        myStopwatch.start("indexer.run()/scores")
        
        #todo remove
        tfdf.cache()
        tfdf.map(lambda x: x[1][1])

        scores = tfdf.map(update)\
            .sortByKey(ascending=False)\
            .map(lambda x:x[1])\
            .persist(pyspark.StorageLevel(True, True, False, False, 1))
        myStopwatch.stop()

        # create final docindex

        # get comments
        def getIdDocTuple(x):
            d = json.loads(x)
            return d["id"], d["body"]
        texts = comments.map(getIdDocTuple)

        #get reactions
        def getIdDocTuple2(x):
            d = json.loads(x)
            s = int(d["score"])*(1+int(d["gilded"]))
            results = re.finditer(r"xkcd\.com/[0-9]+", d['body'])
            unique = set('http://'+i.group(0) for i in results)
            body = [(x,s) for x in unique] # Looks like [ (XKCD_ID, 1), ... 
            return d["parent_id"][3:], body
        
        texts2 = sc.textFile(self.inPathReactions, use_unicode=True)\
            .map(getIdDocTuple2)\
            .combineByKey(list, operator.add, operator.add)

        # transpose termindex to docindex and merged with comments and
        # reactions
        def transpose(x):
            term, postings = x
            l = []
            for doc_id, doc_score in postings:
                l.append((doc_id, (term[0], doc_score)))
            return l
        scoresByDoc = scores.flatMap(transpose)\
            .groupByKey()\
            .mapValues(list)\
            .mapValues(sorted)\
            .join(texts)\
            .join(texts2)\
            .map(lambda x: (x[0], x[1][0][0], x[1][0][1], x[1][1]))\
            .sortBy(lambda x: len(x[1]), ascending=False).cache()

        
        tl = tfdf.map(lambda x: (x[0], [i for i in list(x[1][0])], list(x[1])[1] ))
        #_all = tfdf.map
        #{"df":df.collect()}#, "df":df.collect(), "tfdf":tfdf.map(lambda x: (x[0], (x[1][0], list(x[1][1])))).collect()}#,"scores":scores.collect(), "scoresByDoc":scoresByDoc.collect(), "N":N, "L_avg":L_avg}
        stringy = ""
        for i in scores.toLocalIterator():
            stringy += str(i) + "\n"
        print(stringy)
        #print(json.dumps(tl.collect, ensure_ascii=False, indent=3))

        # output to file

        if not os.path.isdir(self.outPath):
            os.makedirs(self.outPath)

        with zipfile.ZipFile(self.outPath + "/" + "index_term.zip", 'a',
                             zipfile.ZIP_DEFLATED) as f:
            for x in scores.toLocalIterator():
                filename = hashlib.sha1(codecs.encode(x[0][0], "utf-8"))
                filename = str(filename.hexdigest()) + "_i.txt"

                content = json.dumps(x[0], ensure_ascii=False) + "\n"
                content += "\n".join((json.dumps(a, ensure_ascii=False)
                                      for a in x[1]))
                a = codecs.encode(content, "utf-8", errors="surrogateescape")
                f.writestr(filename, a)

        with zipfile.ZipFile(self.outPath + "/" + "index_doc.zip", 'a',
                             zipfile.ZIP_DEFLATED) as f:
            for x in scoresByDoc.toLocalIterator():
                filename = x[0] + "_i.txt"
                content = json.dumps(x, ensure_ascii=False)
                a = codecs.encode(content, "utf-8", errors="surrogateescape")
                f.writestr(filename, a)

        sc.stop()
