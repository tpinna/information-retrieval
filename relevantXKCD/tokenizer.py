from .libraries import porter2 as porter2
from bs4 import BeautifulSoup
from relevantXKCD.libraries.word_tokenize import word_tokenize
from markdown import markdown
import json


def tokenize_comment(comment):
    """Tokenize the body of the comment.

    Arguments:
        comment: str -- a raw comment in json format
    Return value:
        a list of ((term, comment_id, comment_len), 1)-tuples
        note: term is a stemmed token
    """
    d = json.loads(comment)
    terms = tokenize(d['body'])
    return (((term, d['id'], len(terms)), 1) for term in terms)


def tokenize(text):
    """Return a list of terms"""

    # text may contain markdown markup => remove it
    text = markdown(text.lower())
    text = ''.join(BeautifulSoup(text, "html.parser").findAll(text=True))

    tokens = word_tokenize(text)
    terms = [porter2.stem(token) for token in tokens]
    return terms
