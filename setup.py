#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""setup.py: setuptools control."""


from setuptools import setup

with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")

setup(
    name = "RelevantXKCD",
    packages = ["relevantXKCD", "relevantXKCD.libraries"],
    entry_points = {
        "console_scripts": ['xkcd = relevantXKCD.relevantXKCD:main']
        },
    version = "1.0",
    description = "Python command line application to find relevant XKCD comics",
    long_description = long_descr,
    author = "Thomas, Ted, Glenn",
    author_email = "",
    url = "http://",
    install_requires = [
        'markdown',
        'bs4',
        'numpy',
        'psutil'
        ],
    package_data = {
        'relevantXKCD': ['index/index_doc.zip', 'index/index_term.zip'],
        'relevantXKCD.libraries': ['english.pickle']
        }
    )
